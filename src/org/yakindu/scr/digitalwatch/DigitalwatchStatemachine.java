package org.yakindu.scr.digitalwatch;
import org.yakindu.scr.ITimer;

public class DigitalwatchStatemachine implements IDigitalwatchStatemachine {

	private final boolean[] timeEvents = new boolean[15];

	private final class SCIButtonsImpl implements SCIButtons {

		private boolean topLeftPressed;

		public void raiseTopLeftPressed() {
			topLeftPressed = true;
		}

		private boolean topLeftReleased;

		public void raiseTopLeftReleased() {
			topLeftReleased = true;
		}

		private boolean topRightPressed;

		public void raiseTopRightPressed() {
			topRightPressed = true;
		}

		private boolean topRightReleased;

		public void raiseTopRightReleased() {
			topRightReleased = true;
		}

		private boolean bottomLeftPressed;

		public void raiseBottomLeftPressed() {
			bottomLeftPressed = true;
		}

		private boolean bottomLeftReleased;

		public void raiseBottomLeftReleased() {
			bottomLeftReleased = true;
		}

		private boolean bottomRightPressed;

		public void raiseBottomRightPressed() {
			bottomRightPressed = true;
		}

		private boolean bottomRightReleased;

		public void raiseBottomRightReleased() {
			bottomRightReleased = true;
		}

		public void clearEvents() {
			topLeftPressed = false;
			topLeftReleased = false;
			topRightPressed = false;
			topRightReleased = false;
			bottomLeftPressed = false;
			bottomLeftReleased = false;
			bottomRightPressed = false;
			bottomRightReleased = false;
		}

	}

	private SCIButtonsImpl sCIButtons;
	private final class SCIDisplayImpl implements SCIDisplay {

		private SCIDisplayOperationCallback operationCallback;

		public void setSCIDisplayOperationCallback(
				SCIDisplayOperationCallback operationCallback) {
			this.operationCallback = operationCallback;
		}

	}

	private SCIDisplayImpl sCIDisplay;
	private final class SCILogicUnitImpl implements SCILogicUnit {

		private SCILogicUnitOperationCallback operationCallback;

		public void setSCILogicUnitOperationCallback(
				SCILogicUnitOperationCallback operationCallback) {
			this.operationCallback = operationCallback;
		}

		private boolean startAlarm;

		public void raiseStartAlarm() {
			startAlarm = true;
		}

		public void clearEvents() {
			startAlarm = false;
		}

	}

	private SCILogicUnitImpl sCILogicUnit;

	public enum State {
		main_region_watch, main_region_watch_view_TimeDisplay, main_region_watch_view_ChronoDisplay, main_region_watch_view_TimeEditDisplay, main_region_watch_backlight_switch_ButtonIdle, main_region_watch_backlight_switch_ButtonHeld, main_region_watch_backlight_switch_ButtonReleased, main_region_watch_exitedit_switch_ButtonIdle, main_region_watch_exitedit_switch_ButtonHeld, main_region_watch_exitedit_switch_Event, main_region_watch_exitedit_switch_selectNext, main_region_watch_alarm_Off, main_region_watch_alarm_Blinking, main_region_watch_alarm_Blinking_blinking_modes_BlinkOff, main_region_watch_alarm_Blinking_blinking_modes_BlinkOn, main_region_watch_time_editmode_Normal, main_region_watch_time_editmode_EditingTime, main_region_watch_time_editmode_EditingAlarm, main_region_watch_alarmediting_switch_ButtonIdle, main_region_watch_alarmediting_switch_ButtonHeld, main_region_watch_alarmediting_switch_Event, main_region_watch_editing_blinking_Off, main_region_watch_editing_blinking_Blinking, main_region_watch_editing_blinking_Blinking_blink_state_BlinkOn, main_region_watch_editing_blinking_Blinking_blink_state_BlinkOff, main_region_watch_editing_changes_off, main_region_watch_editing_changes_Editing, main_region_watch_editing_changes_ButtonPressedBriefly, main_region_watch_editing_changes_ButtonHeld, main_region_watch_timeediting_switch_ButtonIdle, main_region_watch_timeediting_switch_ButtonHeld, main_region_watch_timeediting_switch_Event, main_region_watch_chrono_Normal, main_region_watch_chrono_Off, main_region_watch_chrono_On, main_region_watch_ticking_TimeTickingMode, main_region_watch_ticking_TimeEditingMode, $NullState$
	};

	private long blinks;
	private long view;
	private long editmode;
	private boolean timeEditingStarted;
	private boolean alarmEditingStarted;
	private boolean normalTimeResumed;
	private long editingModeCounter;

	private final State[] stateVector = new State[11];

	private int nextStateIndex;

	private ITimer timer;

	static {
	}

	public DigitalwatchStatemachine() {

		sCIButtons = new SCIButtonsImpl();
		sCIDisplay = new SCIDisplayImpl();
		sCILogicUnit = new SCILogicUnitImpl();
	}

	public void init() {
		if (timer == null) {
			throw new IllegalStateException("timer not set.");
		}
		for (int i = 0; i < 11; i++) {
			stateVector[i] = State.$NullState$;
		}

		clearEvents();
		clearOutEvents();

		blinks = 0;

		view = 0;

		editmode = 0;

		timeEditingStarted = false;

		alarmEditingStarted = false;

		normalTimeResumed = false;

		editingModeCounter = 0;
	}

	public void enter() {
		if (timer == null) {
			throw new IllegalStateException("timer not set.");
		}
		entryAction();

		timer.setTimer(this, 0, 10, true);

		view = 0;

		nextStateIndex = 0;
		stateVector[0] = State.main_region_watch_view_TimeDisplay;

		sCIDisplay.operationCallback.unsetIndiglo();

		nextStateIndex = 1;
		stateVector[1] = State.main_region_watch_backlight_switch_ButtonIdle;

		nextStateIndex = 2;
		stateVector[2] = State.main_region_watch_exitedit_switch_ButtonIdle;

		nextStateIndex = 3;
		stateVector[3] = State.main_region_watch_alarm_Off;

		editmode = 0;

		normalTimeResumed = true;

		timeEditingStarted = false;

		alarmEditingStarted = false;

		nextStateIndex = 4;
		stateVector[4] = State.main_region_watch_time_editmode_Normal;

		nextStateIndex = 5;
		stateVector[5] = State.main_region_watch_alarmediting_switch_ButtonIdle;

		nextStateIndex = 6;
		stateVector[6] = State.main_region_watch_editing_blinking_Off;

		editmode = 0;

		nextStateIndex = 7;
		stateVector[7] = State.main_region_watch_editing_changes_off;

		nextStateIndex = 8;
		stateVector[8] = State.main_region_watch_timeediting_switch_ButtonIdle;

		nextStateIndex = 9;
		stateVector[9] = State.main_region_watch_chrono_Normal;

		timer.setTimer(this, 14, 1 * 1000, true);

		nextStateIndex = 10;
		stateVector[10] = State.main_region_watch_ticking_TimeTickingMode;
	}

	public void exit() {
		switch (stateVector[0]) {
			case main_region_watch_view_TimeDisplay :
				nextStateIndex = 0;
				stateVector[0] = State.$NullState$;

				timer.unsetTimer(this, 0);
				break;

			case main_region_watch_view_ChronoDisplay :
				nextStateIndex = 0;
				stateVector[0] = State.$NullState$;

				timer.unsetTimer(this, 1);
				break;

			case main_region_watch_view_TimeEditDisplay :
				nextStateIndex = 0;
				stateVector[0] = State.$NullState$;
				break;

			default :
				break;
		}

		switch (stateVector[1]) {
			case main_region_watch_backlight_switch_ButtonIdle :
				nextStateIndex = 1;
				stateVector[1] = State.$NullState$;
				break;

			case main_region_watch_backlight_switch_ButtonHeld :
				nextStateIndex = 1;
				stateVector[1] = State.$NullState$;
				break;

			case main_region_watch_backlight_switch_ButtonReleased :
				nextStateIndex = 1;
				stateVector[1] = State.$NullState$;

				timer.unsetTimer(this, 2);
				break;

			default :
				break;
		}

		switch (stateVector[2]) {
			case main_region_watch_exitedit_switch_ButtonIdle :
				nextStateIndex = 2;
				stateVector[2] = State.$NullState$;
				break;

			case main_region_watch_exitedit_switch_ButtonHeld :
				nextStateIndex = 2;
				stateVector[2] = State.$NullState$;

				timer.unsetTimer(this, 3);
				break;

			case main_region_watch_exitedit_switch_Event :
				nextStateIndex = 2;
				stateVector[2] = State.$NullState$;
				break;

			case main_region_watch_exitedit_switch_selectNext :
				nextStateIndex = 2;
				stateVector[2] = State.$NullState$;
				break;

			default :
				break;
		}

		switch (stateVector[3]) {
			case main_region_watch_alarm_Off :
				nextStateIndex = 3;
				stateVector[3] = State.$NullState$;
				break;

			case main_region_watch_alarm_Blinking_blinking_modes_BlinkOff :
				nextStateIndex = 3;
				stateVector[3] = State.$NullState$;

				timer.unsetTimer(this, 4);
				break;

			case main_region_watch_alarm_Blinking_blinking_modes_BlinkOn :
				nextStateIndex = 3;
				stateVector[3] = State.$NullState$;

				timer.unsetTimer(this, 5);

				sCIDisplay.operationCallback.unsetIndiglo();
				break;

			default :
				break;
		}

		switch (stateVector[4]) {
			case main_region_watch_time_editmode_Normal :
				nextStateIndex = 4;
				stateVector[4] = State.$NullState$;
				break;

			case main_region_watch_time_editmode_EditingTime :
				nextStateIndex = 4;
				stateVector[4] = State.$NullState$;
				break;

			case main_region_watch_time_editmode_EditingAlarm :
				nextStateIndex = 4;
				stateVector[4] = State.$NullState$;
				break;

			default :
				break;
		}

		switch (stateVector[5]) {
			case main_region_watch_alarmediting_switch_ButtonIdle :
				nextStateIndex = 5;
				stateVector[5] = State.$NullState$;
				break;

			case main_region_watch_alarmediting_switch_ButtonHeld :
				nextStateIndex = 5;
				stateVector[5] = State.$NullState$;

				timer.unsetTimer(this, 6);
				break;

			case main_region_watch_alarmediting_switch_Event :
				nextStateIndex = 5;
				stateVector[5] = State.$NullState$;
				break;

			default :
				break;
		}

		switch (stateVector[6]) {
			case main_region_watch_editing_blinking_Off :
				nextStateIndex = 6;
				stateVector[6] = State.$NullState$;
				break;

			case main_region_watch_editing_blinking_Blinking_blink_state_BlinkOn :
				nextStateIndex = 6;
				stateVector[6] = State.$NullState$;

				timer.unsetTimer(this, 7);
				break;

			case main_region_watch_editing_blinking_Blinking_blink_state_BlinkOff :
				nextStateIndex = 6;
				stateVector[6] = State.$NullState$;

				timer.unsetTimer(this, 8);
				break;

			default :
				break;
		}

		switch (stateVector[7]) {
			case main_region_watch_editing_changes_off :
				nextStateIndex = 7;
				stateVector[7] = State.$NullState$;
				break;

			case main_region_watch_editing_changes_Editing :
				nextStateIndex = 7;
				stateVector[7] = State.$NullState$;

				timer.unsetTimer(this, 9);
				break;

			case main_region_watch_editing_changes_ButtonPressedBriefly :
				nextStateIndex = 7;
				stateVector[7] = State.$NullState$;

				timer.unsetTimer(this, 10);
				break;

			case main_region_watch_editing_changes_ButtonHeld :
				nextStateIndex = 7;
				stateVector[7] = State.$NullState$;

				timer.unsetTimer(this, 11);
				break;

			default :
				break;
		}

		switch (stateVector[8]) {
			case main_region_watch_timeediting_switch_ButtonIdle :
				nextStateIndex = 8;
				stateVector[8] = State.$NullState$;
				break;

			case main_region_watch_timeediting_switch_ButtonHeld :
				nextStateIndex = 8;
				stateVector[8] = State.$NullState$;

				timer.unsetTimer(this, 12);
				break;

			case main_region_watch_timeediting_switch_Event :
				nextStateIndex = 8;
				stateVector[8] = State.$NullState$;
				break;

			default :
				break;
		}

		switch (stateVector[9]) {
			case main_region_watch_chrono_Normal :
				nextStateIndex = 9;
				stateVector[9] = State.$NullState$;
				break;

			case main_region_watch_chrono_Off :
				nextStateIndex = 9;
				stateVector[9] = State.$NullState$;
				break;

			case main_region_watch_chrono_On :
				nextStateIndex = 9;
				stateVector[9] = State.$NullState$;

				timer.unsetTimer(this, 13);
				break;

			default :
				break;
		}

		switch (stateVector[10]) {
			case main_region_watch_ticking_TimeTickingMode :
				nextStateIndex = 10;
				stateVector[10] = State.$NullState$;

				timer.unsetTimer(this, 14);
				break;

			case main_region_watch_ticking_TimeEditingMode :
				nextStateIndex = 10;
				stateVector[10] = State.$NullState$;
				break;

			default :
				break;
		}

		exitAction();
	}

	/**
	 * This method resets the incoming events (time events included).
	 */
	protected void clearEvents() {
		sCIButtons.clearEvents();
		sCILogicUnit.clearEvents();

		for (int i = 0; i < timeEvents.length; i++) {
			timeEvents[i] = false;
		}
	}

	/**
	 * This method resets the outgoing events.
	 */
	protected void clearOutEvents() {
	}

	/**
	 * Returns true if the given state is currently active otherwise false.
	 */
	public boolean isStateActive(State state) {
		switch (state) {
			case main_region_watch :
				return stateVector[0].ordinal() >= State.main_region_watch
						.ordinal()
						&& stateVector[0].ordinal() <= State.main_region_watch_ticking_TimeEditingMode
								.ordinal();
			case main_region_watch_view_TimeDisplay :
				return stateVector[0] == State.main_region_watch_view_TimeDisplay;
			case main_region_watch_view_ChronoDisplay :
				return stateVector[0] == State.main_region_watch_view_ChronoDisplay;
			case main_region_watch_view_TimeEditDisplay :
				return stateVector[0] == State.main_region_watch_view_TimeEditDisplay;
			case main_region_watch_backlight_switch_ButtonIdle :
				return stateVector[1] == State.main_region_watch_backlight_switch_ButtonIdle;
			case main_region_watch_backlight_switch_ButtonHeld :
				return stateVector[1] == State.main_region_watch_backlight_switch_ButtonHeld;
			case main_region_watch_backlight_switch_ButtonReleased :
				return stateVector[1] == State.main_region_watch_backlight_switch_ButtonReleased;
			case main_region_watch_exitedit_switch_ButtonIdle :
				return stateVector[2] == State.main_region_watch_exitedit_switch_ButtonIdle;
			case main_region_watch_exitedit_switch_ButtonHeld :
				return stateVector[2] == State.main_region_watch_exitedit_switch_ButtonHeld;
			case main_region_watch_exitedit_switch_Event :
				return stateVector[2] == State.main_region_watch_exitedit_switch_Event;
			case main_region_watch_exitedit_switch_selectNext :
				return stateVector[2] == State.main_region_watch_exitedit_switch_selectNext;
			case main_region_watch_alarm_Off :
				return stateVector[3] == State.main_region_watch_alarm_Off;
			case main_region_watch_alarm_Blinking :
				return stateVector[3].ordinal() >= State.main_region_watch_alarm_Blinking
						.ordinal()
						&& stateVector[3].ordinal() <= State.main_region_watch_alarm_Blinking_blinking_modes_BlinkOn
								.ordinal();
			case main_region_watch_alarm_Blinking_blinking_modes_BlinkOff :
				return stateVector[3] == State.main_region_watch_alarm_Blinking_blinking_modes_BlinkOff;
			case main_region_watch_alarm_Blinking_blinking_modes_BlinkOn :
				return stateVector[3] == State.main_region_watch_alarm_Blinking_blinking_modes_BlinkOn;
			case main_region_watch_time_editmode_Normal :
				return stateVector[4] == State.main_region_watch_time_editmode_Normal;
			case main_region_watch_time_editmode_EditingTime :
				return stateVector[4] == State.main_region_watch_time_editmode_EditingTime;
			case main_region_watch_time_editmode_EditingAlarm :
				return stateVector[4] == State.main_region_watch_time_editmode_EditingAlarm;
			case main_region_watch_alarmediting_switch_ButtonIdle :
				return stateVector[5] == State.main_region_watch_alarmediting_switch_ButtonIdle;
			case main_region_watch_alarmediting_switch_ButtonHeld :
				return stateVector[5] == State.main_region_watch_alarmediting_switch_ButtonHeld;
			case main_region_watch_alarmediting_switch_Event :
				return stateVector[5] == State.main_region_watch_alarmediting_switch_Event;
			case main_region_watch_editing_blinking_Off :
				return stateVector[6] == State.main_region_watch_editing_blinking_Off;
			case main_region_watch_editing_blinking_Blinking :
				return stateVector[6].ordinal() >= State.main_region_watch_editing_blinking_Blinking
						.ordinal()
						&& stateVector[6].ordinal() <= State.main_region_watch_editing_blinking_Blinking_blink_state_BlinkOff
								.ordinal();
			case main_region_watch_editing_blinking_Blinking_blink_state_BlinkOn :
				return stateVector[6] == State.main_region_watch_editing_blinking_Blinking_blink_state_BlinkOn;
			case main_region_watch_editing_blinking_Blinking_blink_state_BlinkOff :
				return stateVector[6] == State.main_region_watch_editing_blinking_Blinking_blink_state_BlinkOff;
			case main_region_watch_editing_changes_off :
				return stateVector[7] == State.main_region_watch_editing_changes_off;
			case main_region_watch_editing_changes_Editing :
				return stateVector[7] == State.main_region_watch_editing_changes_Editing;
			case main_region_watch_editing_changes_ButtonPressedBriefly :
				return stateVector[7] == State.main_region_watch_editing_changes_ButtonPressedBriefly;
			case main_region_watch_editing_changes_ButtonHeld :
				return stateVector[7] == State.main_region_watch_editing_changes_ButtonHeld;
			case main_region_watch_timeediting_switch_ButtonIdle :
				return stateVector[8] == State.main_region_watch_timeediting_switch_ButtonIdle;
			case main_region_watch_timeediting_switch_ButtonHeld :
				return stateVector[8] == State.main_region_watch_timeediting_switch_ButtonHeld;
			case main_region_watch_timeediting_switch_Event :
				return stateVector[8] == State.main_region_watch_timeediting_switch_Event;
			case main_region_watch_chrono_Normal :
				return stateVector[9] == State.main_region_watch_chrono_Normal;
			case main_region_watch_chrono_Off :
				return stateVector[9] == State.main_region_watch_chrono_Off;
			case main_region_watch_chrono_On :
				return stateVector[9] == State.main_region_watch_chrono_On;
			case main_region_watch_ticking_TimeTickingMode :
				return stateVector[10] == State.main_region_watch_ticking_TimeTickingMode;
			case main_region_watch_ticking_TimeEditingMode :
				return stateVector[10] == State.main_region_watch_ticking_TimeEditingMode;
			default :
				return false;
		}
	}

	/**
	 * Set the {@link ITimer} for the state machine. It must be set
	 * externally on a timed state machine before a run cycle can be correct
	 * executed.
	 * 
	 * @param timer
	 */
	public void setTimer(ITimer timer) {
		this.timer = timer;
	}

	/**
	 * Returns the currently used timer.
	 * 
	 * @return {@link ITimer}
	 */
	public ITimer getTimer() {
		return timer;
	}

	public void timeElapsed(int eventID) {
		timeEvents[eventID] = true;
	}

	public SCIButtons getSCIButtons() {
		return sCIButtons;
	}
	public SCIDisplay getSCIDisplay() {
		return sCIDisplay;
	}
	public SCILogicUnit getSCILogicUnit() {
		return sCILogicUnit;
	}

	/* Entry action for statechart 'digitalwatch'. */
	private void entryAction() {
	}

	/* Exit action for state 'digitalwatch'. */
	private void exitAction() {
	}

	/* The reactions of state TimeDisplay. */
	private void reactMain_region_watch_view_TimeDisplay() {
		if (sCIButtons.topLeftReleased) {
			nextStateIndex = 0;
			stateVector[0] = State.$NullState$;

			timer.unsetTimer(this, 0);

			timer.setTimer(this, 1, 10, true);

			view = 2;

			nextStateIndex = 0;
			stateVector[0] = State.main_region_watch_view_ChronoDisplay;
		} else {
			if (alarmEditingStarted) {
				nextStateIndex = 0;
				stateVector[0] = State.$NullState$;

				timer.unsetTimer(this, 0);

				view = 1;

				nextStateIndex = 0;
				stateVector[0] = State.main_region_watch_view_TimeEditDisplay;
			} else {
				if (timeEditingStarted) {
					nextStateIndex = 0;
					stateVector[0] = State.$NullState$;

					timer.unsetTimer(this, 0);

					view = 1;

					nextStateIndex = 0;
					stateVector[0] = State.main_region_watch_view_TimeEditDisplay;
				} else {
					if (timeEvents[0]) {
						sCIDisplay.operationCallback.refreshAlarmDisplay();

						sCIDisplay.operationCallback.refreshDateDisplay();

						sCIDisplay.operationCallback.refreshTimeDisplay();
					}
				}
			}
		}
	}

	/* The reactions of state ChronoDisplay. */
	private void reactMain_region_watch_view_ChronoDisplay() {
		if (sCIButtons.topLeftReleased) {
			nextStateIndex = 0;
			stateVector[0] = State.$NullState$;

			timer.unsetTimer(this, 1);

			timer.setTimer(this, 0, 10, true);

			view = 0;

			nextStateIndex = 0;
			stateVector[0] = State.main_region_watch_view_TimeDisplay;
		} else {
			if (timeEvents[1]) {
				sCIDisplay.operationCallback.refreshAlarmDisplay();

				sCIDisplay.operationCallback.refreshDateDisplay();

				sCIDisplay.operationCallback.refreshChronoDisplay();
			}
		}
	}

	/* The reactions of state TimeEditDisplay. */
	private void reactMain_region_watch_view_TimeEditDisplay() {
		if (normalTimeResumed) {
			nextStateIndex = 0;
			stateVector[0] = State.$NullState$;

			timer.setTimer(this, 0, 10, true);

			view = 0;

			nextStateIndex = 0;
			stateVector[0] = State.main_region_watch_view_TimeDisplay;
		}
	}

	/* The reactions of state ButtonIdle. */
	private void reactMain_region_watch_backlight_switch_ButtonIdle() {
		if (sCIButtons.topRightPressed) {
			nextStateIndex = 1;
			stateVector[1] = State.$NullState$;

			sCIDisplay.operationCallback.setIndiglo();

			nextStateIndex = 1;
			stateVector[1] = State.main_region_watch_backlight_switch_ButtonHeld;
		}
	}

	/* The reactions of state ButtonHeld. */
	private void reactMain_region_watch_backlight_switch_ButtonHeld() {
		if (sCIButtons.topRightReleased) {
			nextStateIndex = 1;
			stateVector[1] = State.$NullState$;

			timer.setTimer(this, 2, 2 * 1000, false);

			nextStateIndex = 1;
			stateVector[1] = State.main_region_watch_backlight_switch_ButtonReleased;
		}
	}

	/* The reactions of state ButtonReleased. */
	private void reactMain_region_watch_backlight_switch_ButtonReleased() {
		if (timeEvents[2]) {
			nextStateIndex = 1;
			stateVector[1] = State.$NullState$;

			timer.unsetTimer(this, 2);

			sCIDisplay.operationCallback.unsetIndiglo();

			nextStateIndex = 1;
			stateVector[1] = State.main_region_watch_backlight_switch_ButtonIdle;
		}
	}

	/* The reactions of state ButtonIdle. */
	private void reactMain_region_watch_exitedit_switch_ButtonIdle() {
		if ((sCIButtons.bottomRightPressed) && (view == 1 && editmode != 0)) {
			nextStateIndex = 2;
			stateVector[2] = State.$NullState$;

			timer.setTimer(this, 3, 2 * 1000, false);

			nextStateIndex = 2;
			stateVector[2] = State.main_region_watch_exitedit_switch_ButtonHeld;
		}
	}

	/* The reactions of state ButtonHeld. */
	private void reactMain_region_watch_exitedit_switch_ButtonHeld() {
		if (timeEvents[3]) {
			nextStateIndex = 2;
			stateVector[2] = State.$NullState$;

			timer.unsetTimer(this, 3);

			normalTimeResumed = true;

			nextStateIndex = 2;
			stateVector[2] = State.main_region_watch_exitedit_switch_Event;
		} else {
			if (sCIButtons.bottomRightReleased) {
				nextStateIndex = 2;
				stateVector[2] = State.$NullState$;

				timer.unsetTimer(this, 3);

				sCILogicUnit.operationCallback.selectNext();

				editingModeCounter = 5;

				nextStateIndex = 2;
				stateVector[2] = State.main_region_watch_exitedit_switch_selectNext;
			}
		}
	}

	/* The reactions of state Event. */
	private void reactMain_region_watch_exitedit_switch_Event() {
		nextStateIndex = 2;
		stateVector[2] = State.$NullState$;

		nextStateIndex = 2;
		stateVector[2] = State.main_region_watch_exitedit_switch_ButtonIdle;
	}

	/* The reactions of state selectNext. */
	private void reactMain_region_watch_exitedit_switch_selectNext() {
		nextStateIndex = 2;
		stateVector[2] = State.$NullState$;

		nextStateIndex = 2;
		stateVector[2] = State.main_region_watch_exitedit_switch_ButtonIdle;
	}

	/* The reactions of state Off. */
	private void reactMain_region_watch_alarm_Off() {
		if (sCILogicUnit.startAlarm) {
			nextStateIndex = 3;
			stateVector[3] = State.$NullState$;

			timer.setTimer(this, 5, 500, false);

			sCIDisplay.operationCallback.setIndiglo();

			blinks = blinks + 1;

			nextStateIndex = 3;
			stateVector[3] = State.main_region_watch_alarm_Blinking_blinking_modes_BlinkOn;
		}
	}

	/* The reactions of state BlinkOff. */
	private void reactMain_region_watch_alarm_Blinking_blinking_modes_BlinkOff() {
		if (sCIButtons.topLeftReleased) {
			switch (stateVector[3]) {
				case main_region_watch_alarm_Blinking_blinking_modes_BlinkOff :
					nextStateIndex = 3;
					stateVector[3] = State.$NullState$;

					timer.unsetTimer(this, 4);
					break;

				case main_region_watch_alarm_Blinking_blinking_modes_BlinkOn :
					nextStateIndex = 3;
					stateVector[3] = State.$NullState$;

					timer.unsetTimer(this, 5);

					sCIDisplay.operationCallback.unsetIndiglo();
					break;

				default :
					break;
			}

			nextStateIndex = 3;
			stateVector[3] = State.main_region_watch_alarm_Off;
		} else {
			if (sCIButtons.topRightReleased) {
				switch (stateVector[3]) {
					case main_region_watch_alarm_Blinking_blinking_modes_BlinkOff :
						nextStateIndex = 3;
						stateVector[3] = State.$NullState$;

						timer.unsetTimer(this, 4);
						break;

					case main_region_watch_alarm_Blinking_blinking_modes_BlinkOn :
						nextStateIndex = 3;
						stateVector[3] = State.$NullState$;

						timer.unsetTimer(this, 5);

						sCIDisplay.operationCallback.unsetIndiglo();
						break;

					default :
						break;
				}

				nextStateIndex = 3;
				stateVector[3] = State.main_region_watch_alarm_Off;
			} else {
				if (sCIButtons.bottomLeftReleased) {
					switch (stateVector[3]) {
						case main_region_watch_alarm_Blinking_blinking_modes_BlinkOff :
							nextStateIndex = 3;
							stateVector[3] = State.$NullState$;

							timer.unsetTimer(this, 4);
							break;

						case main_region_watch_alarm_Blinking_blinking_modes_BlinkOn :
							nextStateIndex = 3;
							stateVector[3] = State.$NullState$;

							timer.unsetTimer(this, 5);

							sCIDisplay.operationCallback.unsetIndiglo();
							break;

						default :
							break;
					}

					nextStateIndex = 3;
					stateVector[3] = State.main_region_watch_alarm_Off;
				} else {
					if (sCIButtons.bottomRightReleased) {
						switch (stateVector[3]) {
							case main_region_watch_alarm_Blinking_blinking_modes_BlinkOff :
								nextStateIndex = 3;
								stateVector[3] = State.$NullState$;

								timer.unsetTimer(this, 4);
								break;

							case main_region_watch_alarm_Blinking_blinking_modes_BlinkOn :
								nextStateIndex = 3;
								stateVector[3] = State.$NullState$;

								timer.unsetTimer(this, 5);

								sCIDisplay.operationCallback.unsetIndiglo();
								break;

							default :
								break;
						}

						nextStateIndex = 3;
						stateVector[3] = State.main_region_watch_alarm_Off;
					} else {
						if (timeEvents[4]) {
							nextStateIndex = 3;
							stateVector[3] = State.$NullState$;

							timer.unsetTimer(this, 4);

							timer.setTimer(this, 5, 500, false);

							sCIDisplay.operationCallback.setIndiglo();

							blinks = blinks + 1;

							nextStateIndex = 3;
							stateVector[3] = State.main_region_watch_alarm_Blinking_blinking_modes_BlinkOn;
						}
					}
				}
			}
		}
	}

	/* The reactions of state BlinkOn. */
	private void reactMain_region_watch_alarm_Blinking_blinking_modes_BlinkOn() {
		if (sCIButtons.topLeftReleased) {
			switch (stateVector[3]) {
				case main_region_watch_alarm_Blinking_blinking_modes_BlinkOff :
					nextStateIndex = 3;
					stateVector[3] = State.$NullState$;

					timer.unsetTimer(this, 4);
					break;

				case main_region_watch_alarm_Blinking_blinking_modes_BlinkOn :
					nextStateIndex = 3;
					stateVector[3] = State.$NullState$;

					timer.unsetTimer(this, 5);

					sCIDisplay.operationCallback.unsetIndiglo();
					break;

				default :
					break;
			}

			nextStateIndex = 3;
			stateVector[3] = State.main_region_watch_alarm_Off;
		} else {
			if (sCIButtons.topRightReleased) {
				switch (stateVector[3]) {
					case main_region_watch_alarm_Blinking_blinking_modes_BlinkOff :
						nextStateIndex = 3;
						stateVector[3] = State.$NullState$;

						timer.unsetTimer(this, 4);
						break;

					case main_region_watch_alarm_Blinking_blinking_modes_BlinkOn :
						nextStateIndex = 3;
						stateVector[3] = State.$NullState$;

						timer.unsetTimer(this, 5);

						sCIDisplay.operationCallback.unsetIndiglo();
						break;

					default :
						break;
				}

				nextStateIndex = 3;
				stateVector[3] = State.main_region_watch_alarm_Off;
			} else {
				if (sCIButtons.bottomLeftReleased) {
					switch (stateVector[3]) {
						case main_region_watch_alarm_Blinking_blinking_modes_BlinkOff :
							nextStateIndex = 3;
							stateVector[3] = State.$NullState$;

							timer.unsetTimer(this, 4);
							break;

						case main_region_watch_alarm_Blinking_blinking_modes_BlinkOn :
							nextStateIndex = 3;
							stateVector[3] = State.$NullState$;

							timer.unsetTimer(this, 5);

							sCIDisplay.operationCallback.unsetIndiglo();
							break;

						default :
							break;
					}

					nextStateIndex = 3;
					stateVector[3] = State.main_region_watch_alarm_Off;
				} else {
					if (sCIButtons.bottomRightReleased) {
						switch (stateVector[3]) {
							case main_region_watch_alarm_Blinking_blinking_modes_BlinkOff :
								nextStateIndex = 3;
								stateVector[3] = State.$NullState$;

								timer.unsetTimer(this, 4);
								break;

							case main_region_watch_alarm_Blinking_blinking_modes_BlinkOn :
								nextStateIndex = 3;
								stateVector[3] = State.$NullState$;

								timer.unsetTimer(this, 5);

								sCIDisplay.operationCallback.unsetIndiglo();
								break;

							default :
								break;
						}

						nextStateIndex = 3;
						stateVector[3] = State.main_region_watch_alarm_Off;
					} else {
						if (timeEvents[5]) {
							nextStateIndex = 3;
							stateVector[3] = State.$NullState$;

							timer.unsetTimer(this, 5);

							sCIDisplay.operationCallback.unsetIndiglo();

							if (blinks < 4) {

								timer.setTimer(this, 4, 500, false);

								nextStateIndex = 3;
								stateVector[3] = State.main_region_watch_alarm_Blinking_blinking_modes_BlinkOff;
							} else {
								switch (stateVector[3]) {
									case main_region_watch_alarm_Blinking_blinking_modes_BlinkOff :
										nextStateIndex = 3;
										stateVector[3] = State.$NullState$;

										timer.unsetTimer(this, 4);
										break;

									case main_region_watch_alarm_Blinking_blinking_modes_BlinkOn :
										nextStateIndex = 3;
										stateVector[3] = State.$NullState$;

										timer.unsetTimer(this, 5);

										sCIDisplay.operationCallback
												.unsetIndiglo();
										break;

									default :
										break;
								}

								nextStateIndex = 3;
								stateVector[3] = State.main_region_watch_alarm_Off;
							}
						}
					}
				}
			}
		}
	}

	/* The reactions of state Normal. */
	private void reactMain_region_watch_time_editmode_Normal() {
		if (timeEditingStarted) {
			nextStateIndex = 4;
			stateVector[4] = State.$NullState$;

			editmode = 2;

			sCILogicUnit.operationCallback.startTimeEditMode();

			nextStateIndex = 4;
			stateVector[4] = State.main_region_watch_time_editmode_EditingTime;
		} else {
			if (alarmEditingStarted) {
				nextStateIndex = 4;
				stateVector[4] = State.$NullState$;

				editmode = 1;

				sCILogicUnit.operationCallback.startAlarmEditMode();

				nextStateIndex = 4;
				stateVector[4] = State.main_region_watch_time_editmode_EditingAlarm;
			}
		}
	}

	/* The reactions of state EditingTime. */
	private void reactMain_region_watch_time_editmode_EditingTime() {
		if (editingModeCounter == 0 || normalTimeResumed) {
			nextStateIndex = 4;
			stateVector[4] = State.$NullState$;

			editmode = 0;

			normalTimeResumed = true;

			timeEditingStarted = false;

			alarmEditingStarted = false;

			nextStateIndex = 4;
			stateVector[4] = State.main_region_watch_time_editmode_Normal;
		}
	}

	/* The reactions of state EditingAlarm. */
	private void reactMain_region_watch_time_editmode_EditingAlarm() {
		if (editingModeCounter == 0 || normalTimeResumed) {
			nextStateIndex = 4;
			stateVector[4] = State.$NullState$;

			editmode = 0;

			normalTimeResumed = true;

			timeEditingStarted = false;

			alarmEditingStarted = false;

			nextStateIndex = 4;
			stateVector[4] = State.main_region_watch_time_editmode_Normal;
		}
	}

	/* The reactions of state ButtonIdle. */
	private void reactMain_region_watch_alarmediting_switch_ButtonIdle() {
		if ((sCIButtons.bottomLeftPressed) && (view == 0 && editmode == 0)) {
			nextStateIndex = 5;
			stateVector[5] = State.$NullState$;

			timer.setTimer(this, 6, 1500, false);

			nextStateIndex = 5;
			stateVector[5] = State.main_region_watch_alarmediting_switch_ButtonHeld;
		}
	}

	/* The reactions of state ButtonHeld. */
	private void reactMain_region_watch_alarmediting_switch_ButtonHeld() {
		if ((sCIButtons.bottomLeftReleased) && view == 0) {
			nextStateIndex = 5;
			stateVector[5] = State.$NullState$;

			timer.unsetTimer(this, 6);

			sCILogicUnit.operationCallback.setAlarm();

			nextStateIndex = 5;
			stateVector[5] = State.main_region_watch_alarmediting_switch_ButtonIdle;
		} else {
			if (timeEvents[6]) {
				nextStateIndex = 5;
				stateVector[5] = State.$NullState$;

				timer.unsetTimer(this, 6);

				normalTimeResumed = false;

				alarmEditingStarted = true;

				nextStateIndex = 5;
				stateVector[5] = State.main_region_watch_alarmediting_switch_Event;
			}
		}
	}

	/* The reactions of state Event. */
	private void reactMain_region_watch_alarmediting_switch_Event() {
		nextStateIndex = 5;
		stateVector[5] = State.$NullState$;

		nextStateIndex = 5;
		stateVector[5] = State.main_region_watch_alarmediting_switch_ButtonIdle;
	}

	/* The reactions of state Off. */
	private void reactMain_region_watch_editing_blinking_Off() {
		if (view == 1) {
			nextStateIndex = 6;
			stateVector[6] = State.$NullState$;

			timer.setTimer(this, 7, 400, false);

			sCIDisplay.operationCallback.showSelection();

			nextStateIndex = 6;
			stateVector[6] = State.main_region_watch_editing_blinking_Blinking_blink_state_BlinkOn;
		}
	}

	/* The reactions of state BlinkOn. */
	private void reactMain_region_watch_editing_blinking_Blinking_blink_state_BlinkOn() {
		if (view != 1) {
			switch (stateVector[6]) {
				case main_region_watch_editing_blinking_Blinking_blink_state_BlinkOn :
					nextStateIndex = 6;
					stateVector[6] = State.$NullState$;

					timer.unsetTimer(this, 7);
					break;

				case main_region_watch_editing_blinking_Blinking_blink_state_BlinkOff :
					nextStateIndex = 6;
					stateVector[6] = State.$NullState$;

					timer.unsetTimer(this, 8);
					break;

				default :
					break;
			}

			nextStateIndex = 6;
			stateVector[6] = State.main_region_watch_editing_blinking_Off;
		} else {
			if (timeEvents[7]) {
				nextStateIndex = 6;
				stateVector[6] = State.$NullState$;

				timer.unsetTimer(this, 7);

				timer.setTimer(this, 8, 400, false);

				sCIDisplay.operationCallback.hideSelection();

				nextStateIndex = 6;
				stateVector[6] = State.main_region_watch_editing_blinking_Blinking_blink_state_BlinkOff;
			}
		}
	}

	/* The reactions of state BlinkOff. */
	private void reactMain_region_watch_editing_blinking_Blinking_blink_state_BlinkOff() {
		if (view != 1) {
			switch (stateVector[6]) {
				case main_region_watch_editing_blinking_Blinking_blink_state_BlinkOn :
					nextStateIndex = 6;
					stateVector[6] = State.$NullState$;

					timer.unsetTimer(this, 7);
					break;

				case main_region_watch_editing_blinking_Blinking_blink_state_BlinkOff :
					nextStateIndex = 6;
					stateVector[6] = State.$NullState$;

					timer.unsetTimer(this, 8);
					break;

				default :
					break;
			}

			nextStateIndex = 6;
			stateVector[6] = State.main_region_watch_editing_blinking_Off;
		} else {
			if (timeEvents[8]) {
				nextStateIndex = 6;
				stateVector[6] = State.$NullState$;

				timer.unsetTimer(this, 8);

				timer.setTimer(this, 7, 400, false);

				sCIDisplay.operationCallback.showSelection();

				nextStateIndex = 6;
				stateVector[6] = State.main_region_watch_editing_blinking_Blinking_blink_state_BlinkOn;
			}
		}
	}

	/* The reactions of state off. */
	private void reactMain_region_watch_editing_changes_off() {
		if (editmode != 0) {
			nextStateIndex = 7;
			stateVector[7] = State.$NullState$;

			timer.setTimer(this, 9, 1 * 1000, true);

			editingModeCounter = 5;

			nextStateIndex = 7;
			stateVector[7] = State.main_region_watch_editing_changes_Editing;
		}
	}

	/* The reactions of state Editing. */
	private void reactMain_region_watch_editing_changes_Editing() {
		if (editmode == 0 || editingModeCounter == 0) {
			nextStateIndex = 7;
			stateVector[7] = State.$NullState$;

			timer.unsetTimer(this, 9);

			editmode = 0;

			nextStateIndex = 7;
			stateVector[7] = State.main_region_watch_editing_changes_off;
		} else {
			if (sCIButtons.bottomLeftPressed) {
				nextStateIndex = 7;
				stateVector[7] = State.$NullState$;

				timer.unsetTimer(this, 9);

				timer.setTimer(this, 10, 300, false);

				nextStateIndex = 7;
				stateVector[7] = State.main_region_watch_editing_changes_ButtonPressedBriefly;
			} else {
				if (timeEvents[9]) {
					editingModeCounter = editingModeCounter - 1;
				}
			}
		}
	}

	/* The reactions of state ButtonPressedBriefly. */
	private void reactMain_region_watch_editing_changes_ButtonPressedBriefly() {
		if (sCIButtons.bottomLeftReleased) {
			nextStateIndex = 7;
			stateVector[7] = State.$NullState$;

			timer.unsetTimer(this, 10);

			sCILogicUnit.operationCallback.increaseSelection();

			timer.setTimer(this, 9, 1 * 1000, true);

			editingModeCounter = 5;

			nextStateIndex = 7;
			stateVector[7] = State.main_region_watch_editing_changes_Editing;
		} else {
			if (timeEvents[10]) {
				nextStateIndex = 7;
				stateVector[7] = State.$NullState$;

				timer.unsetTimer(this, 10);

				sCILogicUnit.operationCallback.increaseSelection();

				timer.setTimer(this, 11, 300, true);

				nextStateIndex = 7;
				stateVector[7] = State.main_region_watch_editing_changes_ButtonHeld;
			}
		}
	}

	/* The reactions of state ButtonHeld. */
	private void reactMain_region_watch_editing_changes_ButtonHeld() {
		if (sCIButtons.bottomLeftReleased) {
			nextStateIndex = 7;
			stateVector[7] = State.$NullState$;

			timer.unsetTimer(this, 11);

			timer.setTimer(this, 9, 1 * 1000, true);

			editingModeCounter = 5;

			nextStateIndex = 7;
			stateVector[7] = State.main_region_watch_editing_changes_Editing;
		} else {
			if (timeEvents[11]) {
				sCILogicUnit.operationCallback.increaseSelection();
			}
		}
	}

	/* The reactions of state ButtonIdle. */
	private void reactMain_region_watch_timeediting_switch_ButtonIdle() {
		if ((sCIButtons.bottomRightPressed) && (view == 0 && editmode == 0)) {
			nextStateIndex = 8;
			stateVector[8] = State.$NullState$;

			timer.setTimer(this, 12, 1500, false);

			nextStateIndex = 8;
			stateVector[8] = State.main_region_watch_timeediting_switch_ButtonHeld;
		}
	}

	/* The reactions of state ButtonHeld. */
	private void reactMain_region_watch_timeediting_switch_ButtonHeld() {
		if ((sCIButtons.bottomRightReleased) && view == 0) {
			nextStateIndex = 8;
			stateVector[8] = State.$NullState$;

			timer.unsetTimer(this, 12);

			nextStateIndex = 8;
			stateVector[8] = State.main_region_watch_timeediting_switch_ButtonIdle;
		} else {
			if (timeEvents[12]) {
				nextStateIndex = 8;
				stateVector[8] = State.$NullState$;

				timer.unsetTimer(this, 12);

				normalTimeResumed = false;

				timeEditingStarted = true;

				nextStateIndex = 8;
				stateVector[8] = State.main_region_watch_timeediting_switch_Event;
			}
		}
	}

	/* The reactions of state Event. */
	private void reactMain_region_watch_timeediting_switch_Event() {
		nextStateIndex = 8;
		stateVector[8] = State.$NullState$;

		nextStateIndex = 8;
		stateVector[8] = State.main_region_watch_timeediting_switch_ButtonIdle;
	}

	/* The reactions of state Normal. */
	private void reactMain_region_watch_chrono_Normal() {
		if (view == 2) {
			nextStateIndex = 9;
			stateVector[9] = State.$NullState$;

			nextStateIndex = 9;
			stateVector[9] = State.main_region_watch_chrono_Off;
		}
	}

	/* The reactions of state Off. */
	private void reactMain_region_watch_chrono_Off() {
		if ((sCIButtons.bottomRightReleased) && view == 2) {
			nextStateIndex = 9;
			stateVector[9] = State.$NullState$;

			timer.setTimer(this, 13, 10, true);

			nextStateIndex = 9;
			stateVector[9] = State.main_region_watch_chrono_On;
		} else {
			if ((sCIButtons.bottomLeftReleased) && view == 2) {
				nextStateIndex = 9;
				stateVector[9] = State.$NullState$;

				sCILogicUnit.operationCallback.resetChrono();

				nextStateIndex = 9;
				stateVector[9] = State.main_region_watch_chrono_Off;
			}
		}
	}

	/* The reactions of state On. */
	private void reactMain_region_watch_chrono_On() {
		if ((sCIButtons.bottomRightReleased) && view == 2) {
			nextStateIndex = 9;
			stateVector[9] = State.$NullState$;

			timer.unsetTimer(this, 13);

			nextStateIndex = 9;
			stateVector[9] = State.main_region_watch_chrono_Off;
		} else {
			if ((sCIButtons.bottomLeftReleased) && view == 2) {
				nextStateIndex = 9;
				stateVector[9] = State.$NullState$;

				timer.unsetTimer(this, 13);

				sCILogicUnit.operationCallback.resetChrono();

				timer.setTimer(this, 13, 10, true);

				nextStateIndex = 9;
				stateVector[9] = State.main_region_watch_chrono_On;
			} else {
				if (timeEvents[13]) {
					sCILogicUnit.operationCallback.increaseChronoByOne();
				}
			}
		}
	}

	/* The reactions of state TimeTickingMode. */
	private void reactMain_region_watch_ticking_TimeTickingMode() {
		if (timeEditingStarted) {
			nextStateIndex = 10;
			stateVector[10] = State.$NullState$;

			timer.unsetTimer(this, 14);

			nextStateIndex = 10;
			stateVector[10] = State.main_region_watch_ticking_TimeEditingMode;
		} else {
			if (timeEvents[14]) {
				sCILogicUnit.operationCallback.increaseTimeByOne();
			}
		}
	}

	/* The reactions of state TimeEditingMode. */
	private void reactMain_region_watch_ticking_TimeEditingMode() {
		if (normalTimeResumed) {
			nextStateIndex = 10;
			stateVector[10] = State.$NullState$;

			timer.setTimer(this, 14, 1 * 1000, true);

			nextStateIndex = 10;
			stateVector[10] = State.main_region_watch_ticking_TimeTickingMode;
		}
	}

	public void runCycle() {

		clearOutEvents();

		for (nextStateIndex = 0; nextStateIndex < stateVector.length; nextStateIndex++) {

			switch (stateVector[nextStateIndex]) {
				case main_region_watch_view_TimeDisplay :
					reactMain_region_watch_view_TimeDisplay();
					break;
				case main_region_watch_view_ChronoDisplay :
					reactMain_region_watch_view_ChronoDisplay();
					break;
				case main_region_watch_view_TimeEditDisplay :
					reactMain_region_watch_view_TimeEditDisplay();
					break;
				case main_region_watch_backlight_switch_ButtonIdle :
					reactMain_region_watch_backlight_switch_ButtonIdle();
					break;
				case main_region_watch_backlight_switch_ButtonHeld :
					reactMain_region_watch_backlight_switch_ButtonHeld();
					break;
				case main_region_watch_backlight_switch_ButtonReleased :
					reactMain_region_watch_backlight_switch_ButtonReleased();
					break;
				case main_region_watch_exitedit_switch_ButtonIdle :
					reactMain_region_watch_exitedit_switch_ButtonIdle();
					break;
				case main_region_watch_exitedit_switch_ButtonHeld :
					reactMain_region_watch_exitedit_switch_ButtonHeld();
					break;
				case main_region_watch_exitedit_switch_Event :
					reactMain_region_watch_exitedit_switch_Event();
					break;
				case main_region_watch_exitedit_switch_selectNext :
					reactMain_region_watch_exitedit_switch_selectNext();
					break;
				case main_region_watch_alarm_Off :
					reactMain_region_watch_alarm_Off();
					break;
				case main_region_watch_alarm_Blinking_blinking_modes_BlinkOff :
					reactMain_region_watch_alarm_Blinking_blinking_modes_BlinkOff();
					break;
				case main_region_watch_alarm_Blinking_blinking_modes_BlinkOn :
					reactMain_region_watch_alarm_Blinking_blinking_modes_BlinkOn();
					break;
				case main_region_watch_time_editmode_Normal :
					reactMain_region_watch_time_editmode_Normal();
					break;
				case main_region_watch_time_editmode_EditingTime :
					reactMain_region_watch_time_editmode_EditingTime();
					break;
				case main_region_watch_time_editmode_EditingAlarm :
					reactMain_region_watch_time_editmode_EditingAlarm();
					break;
				case main_region_watch_alarmediting_switch_ButtonIdle :
					reactMain_region_watch_alarmediting_switch_ButtonIdle();
					break;
				case main_region_watch_alarmediting_switch_ButtonHeld :
					reactMain_region_watch_alarmediting_switch_ButtonHeld();
					break;
				case main_region_watch_alarmediting_switch_Event :
					reactMain_region_watch_alarmediting_switch_Event();
					break;
				case main_region_watch_editing_blinking_Off :
					reactMain_region_watch_editing_blinking_Off();
					break;
				case main_region_watch_editing_blinking_Blinking_blink_state_BlinkOn :
					reactMain_region_watch_editing_blinking_Blinking_blink_state_BlinkOn();
					break;
				case main_region_watch_editing_blinking_Blinking_blink_state_BlinkOff :
					reactMain_region_watch_editing_blinking_Blinking_blink_state_BlinkOff();
					break;
				case main_region_watch_editing_changes_off :
					reactMain_region_watch_editing_changes_off();
					break;
				case main_region_watch_editing_changes_Editing :
					reactMain_region_watch_editing_changes_Editing();
					break;
				case main_region_watch_editing_changes_ButtonPressedBriefly :
					reactMain_region_watch_editing_changes_ButtonPressedBriefly();
					break;
				case main_region_watch_editing_changes_ButtonHeld :
					reactMain_region_watch_editing_changes_ButtonHeld();
					break;
				case main_region_watch_timeediting_switch_ButtonIdle :
					reactMain_region_watch_timeediting_switch_ButtonIdle();
					break;
				case main_region_watch_timeediting_switch_ButtonHeld :
					reactMain_region_watch_timeediting_switch_ButtonHeld();
					break;
				case main_region_watch_timeediting_switch_Event :
					reactMain_region_watch_timeediting_switch_Event();
					break;
				case main_region_watch_chrono_Normal :
					reactMain_region_watch_chrono_Normal();
					break;
				case main_region_watch_chrono_Off :
					reactMain_region_watch_chrono_Off();
					break;
				case main_region_watch_chrono_On :
					reactMain_region_watch_chrono_On();
					break;
				case main_region_watch_ticking_TimeTickingMode :
					reactMain_region_watch_ticking_TimeTickingMode();
					break;
				case main_region_watch_ticking_TimeEditingMode :
					reactMain_region_watch_ticking_TimeEditingMode();
					break;
				default :
					// $NullState$
			}
		}

		clearEvents();
	}
}
