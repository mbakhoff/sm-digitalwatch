# README #

## Team members

* Vootele Rõtov
* Märt Bakhoff
* Maarja Lepamets
* Tõnis Pool

## Workflow
The work was done in pairs. At first, Märt and Vootele pair programmed some requirements. The remaining requirements were implemented by Tõnis and Maarja. The pairs worked at different times so we had no issues with merging versions. 

## The Solution

All of the behavior requirements have been implemented with no open issues.

## The Conclusion

Some remarks on the assignment:

1. The first three requirements were rather easy to implemement, making for a good learning curve.
2. The last four where quite more challenging, taking more time and effort to implement.
3. On the 5th requirement we struggeled to find a suitable solution. There seemed to be a semantically suitable design, but we ran into issuses with event scopes as we could not figure out how to make the scope of internal events cover the whole composite state. The solution, that seems to be a standard solution to the problem was using boolean variables, what allowed us to stick with the chosen desing.

Overall, the time and effort required for this homework seemed pretty fair for the points given and the requirements ranged from rather easy to pretty challenging, offering a good learning track. Thumbs up!

## State Modelling in general
State modelling can be a useful tool to visualise some parts of a system for debugging or informative purposes, but modelling a whole working system that has some practical uses will become unpractical pretty fast as the number of possible states and transitions skyrockets quickly. Thus it's a tool that can be useful in some scenarios, for example doing exploratory testing or trying to teach a colleague about a specific part of a system, but not for designing the whole system.